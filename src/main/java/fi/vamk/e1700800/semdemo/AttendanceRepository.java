package fi.vamk.e1700800.semdemo;
import java.util.Date;
import java.util.List;


import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {
    public Attendance findByKey(String key);
    List<Attendance> findAllByDate(Date date);
}